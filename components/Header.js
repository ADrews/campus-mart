import React, { Component } from "react";
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Platform,
  StatusBar
} from "react-native";
import { withNavigation } from "react-navigation";
import { Icon, Left, Right, Header, Item, Input } from "native-base";
class CustomHeader extends Component {
  render() {
    return (
      <View>
        <Header
          style={[
            {
              backgroundColor: "#07485B",
              height: 90,
              borderBottomWidth: 1,
              borderBottomColor: "white"
            },
            styles.androidHeader
          ]}
        >
          <Left style={{ flexDirection: "row", top: 10 }}>
            <TouchableOpacity
              onPress={() => this.props.navigation.openDrawer()}
            >
              <Icon
                name="md-menu"
                style={{
                  color: "white",
                  fontSize: 35,
                  marginRight: 15,
                  marginLeft: 5
                }}
              />
            </TouchableOpacity>
            <Text style={{ color: "white", fontSize: 18 }}>
              Welcome to KiwiMart
            </Text>
          </Left>
          <Right style={{ marginRight: 5, top: 5 }}>
            <Icon name="md-cart" style={{ color: "white", fontSize: 25 }} />
          </Right>
        </Header>
        <View
          style={{
            position: "absolute",
            left: 0,
            right: 0,
            top: 90,
            height: 70,
            backgroundColor: "#07485B",
            flexDirection: "row",
            alignItems: "center",
            paddingHorizontal: 5
          }}
        >
          <View
            style={{
              flex: 1,
              height: "100%",
              justifyContent: "center"
            }}
          >
            <Item
              style={{
                backgroundColor: "white",
                paddingHorizontal: 10,
                borderRadius: 4
              }}
            >
              <Icon name="search" style={{ fontSize: 20, paddingTop: 5 }} />
              <Input placeholder="Search" />
            </Item>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  androidHeader: {
    ...Platform.select({
      android: {
        paddingTop: StatusBar.currentHeight
      }
    })
  }
});

export default withNavigation(CustomHeader);
