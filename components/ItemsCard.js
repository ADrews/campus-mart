import React, { Component } from "react";
import { View, Image, Text } from "react-native";
import { Card, CardItem } from "native-base";

class ItemsCard extends Component {
  render() {
    return (
      <View>
        <View style={{ flexDirection: "row" }}>
          <CardItem>
            <View style={{ alignItems: "center" }}>
              <Image
                style={{ width: 90, height: 90, resizeMode: "contain" }}
                source={require("../assets/Images/provisions.png")}
              />
              <Text>Groceries</Text>
            </View>
          </CardItem>
          <CardItem>
            <View style={{ alignItems: "center" }}>
              <Image
                style={{ width: 90, height: 90, resizeMode: "contain" }}
                source={require("../assets/Images/accessories.png")}
              />
              <Text>Accessories</Text>
            </View>
          </CardItem>
          <CardItem>
            <View style={{ alignItems: "center" }}>
              <Image
                style={{ width: 90, height: 90 }}
                source={require("../assets/Images/item2.png")}
              />
              <Text>Mobiles/Laptops</Text>
            </View>
          </CardItem>
        </View>

        <View style={{ flexDirection: "row" }}>
          <CardItem>
            <View style={{ alignItems: "center" }}>
              <Image
                style={{ width: 90, height: 90 }}
                source={require("../assets/Images/clothes-4.png")}
              />
              <Text>Clothes</Text>
            </View>
          </CardItem>
          <CardItem>
            <View style={{ alignItems: "center" }}>
              <Image
                style={{ width: 90, height: 90 }}
                source={require("../assets/Images/elec.jpg")}
              />
              <Text>Repairs</Text>
            </View>
          </CardItem>
          <CardItem>
            <View style={{ alignItems: "center" }}>
              <Image
                style={{ width: 90, height: 90 }}
                source={require("../assets/Images/shovec.jpg")}
              />
              <Text>Books</Text>
            </View>
          </CardItem>
        </View>
      </View>
    );
  }
}
export default ItemsCard;
