import React, { useRef } from "react";
import {
  View,
  Image,
  Dimensions,
  Text,
  StyleSheet,
  TouchableOpacity,
} from "react-native";
import Carousel from "react-native-anchor-carousel";
const { width } = Dimensions.get("window");

const renderItem = ({ item }) => {
  return (
    <View>
      <TouchableOpacity>
        <Image
          resizeMode="cover"
          source={item.image}
          style={styles.carouselImage}
        />
        <Text style={styles.carouselText}>{item.title}</Text>
      </TouchableOpacity>
    </View>
  );
};

const PomadeCard = ({ data }) => {
  const carouselRef = useRef(null);

  return (
    <View style={styles.carouselContainerView}>
      <Carousel
        style={styles.Carousel}
        data={data}
        renderItem={renderItem}
        itemWidth={200}
        containerWidth={width - 20}
        separatorWidth={0}
        ref={carouselRef}
        inActiveOpacity={0.4}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  Carousel: { flex: 1, overflow: "visible" },
  carouselContainerView: {
    width: "100%",
    height: 200,
    justifyContent: "center",
    alignItems: "center",
  },
  carouselImage: {
    width: 200,
    height: 150,
    alignSelf: "center",
    borderRadius: 10,
    backgroundColor: "black",
  },
  carouselText: {
    padding: 14,
    color: "black",
    position: "absolute",
    bottom: 10,
    fontWeight: "bold",
    left: 2,
  },
  itemStat: {
    paddingLeft: 14,
    color: "white",
    fontWeight: "bold",
    fontSize: 14,
    opacity: 0.8,
  },
  itemName: {
    paddingLeft: 14,
    color: "green",
    fontWeight: "bold",
    fontSize: 20,
    marginBottom: 6,
  },
});

export default PomadeCard;
