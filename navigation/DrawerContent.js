import React, { useEffect, useState } from "react";
import { View, StyleSheet, AsyncStorage } from "react-native";
import {
  useTheme,
  Avatar,
  Title,
  Caption,
  Drawer,
  Text,
  TouchableRipple,
  Switch,
} from "react-native-paper";
import { Alert, TouchableOpacity } from "react-native";
import { DrawerContentScrollView, DrawerItem } from "@react-navigation/drawer";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";
import { IMAGE } from "../constants/Image";
import { AuthContext } from "../components/context";
import { useNavigation } from "@react-navigation/native";


export function DrawerContent({ navigation }) {
  const paperTheme = useTheme();

  const { toggleTheme } = React.useContext(AuthContext);

  const [token1, setToken] = useState("");

  useEffect(()=>{
    async function fetchToken(){
          const token = await AsyncStorage.getItem("token") || "none";
          setToken(token);
    }
    fetchToken();
   
},[]);
  const nav = ()=>{
    if(token1 != "none" || token1 == "" )
    {
     navigation.navigate("Profile")
    }
    else{
         alert("You don't have a profile");
    }
   
  }

  return (
    <View style={{ flex: 1 }}>
      <DrawerContentScrollView>
        <View style={styles.drawerContent}>
          <View style={styles.userInfoSection}>
            <View style={{ flexDirection: "row", marginTop: 15 }}>
              <TouchableOpacity onPress={() => nav()}>
                <Avatar.Image source={IMAGE.avatar} size={70} />
              </TouchableOpacity>
              <View style={{ marginLeft: 15, flexDirection: "column" }}>
                <Title style={styles.title}>User</Title>
                <Caption style={styles.caption}></Caption>
              </View>
            </View>
          </View>

          <Drawer.Section style={styles.drawerSection}>
            <DrawerItem
              icon={() => <Icon name="home-outline" color="grey" size={25} />}
              label="Home"
              labelStyle={{ marginLeft: -16 }}
              onPress={() => {
                navigation.navigate("Home");
              }}
            />
            <DrawerItem
              icon={() => (
                <Icon name="account-outline" color="grey" size={25} />
              )}
              label="Account"
              labelStyle={{ marginLeft: -16 }}
              onPress={() => {
                navigation.navigate("Account");
              }}
            />
            <DrawerItem
              icon={() => <Icon name="database-plus" color="grey" size={25} />}
              label="Products/Services"
              labelStyle={{ marginLeft: -16 }}
              onPress={() => {
                navigation.navigate("Product");
              }}
            />
            <DrawerItem
              icon={() => (
                <Icon name="account-check-outline" color="grey" size={25} />
              )}
              label="About"
              labelStyle={{ marginLeft: -16 }}
              onPress={() => {
                navigation.navigate("About");
              }}
            />
            <DrawerItem
              icon={() => <Icon name="cogs" color="grey" size={25} />}
              label="Settings"
              labelStyle={{ marginLeft: -16 }}
              onPress={() => {
                navigation.navigate("Settings");
              }}
            />
          </Drawer.Section>
          <Drawer.Section title="Preferences">
            <TouchableRipple
              onPress={() => {
                toggleTheme();
              }}
            >
              <View style={styles.preference}>
                <Text>Dark Theme</Text>
                <View pointerEvents="none">
                  <Switch value={paperTheme.dark} />
                </View>
              </View>
            </TouchableRipple>
          </Drawer.Section>
        </View>
      </DrawerContentScrollView>
      <Drawer.Section style={styles.bottomDrawerSection}>
        
      </Drawer.Section>
    </View>
  );
}

const styles = StyleSheet.create({
  drawerContent: {
    flex: 1,
  },
  userInfoSection: {
    paddingLeft: 20,
  },
  title: {
    fontSize: 16,
    marginTop: 3,
    fontWeight: "bold",
  },
  caption: {
    fontSize: 14,
    lineHeight: 14,
  },
  row: {
    marginTop: 20,
    flexDirection: "row",
    alignItems: "center",
  },
  section: {
    flexDirection: "row",
    alignItems: "center",
    marginRight: 15,
  },
  paragraph: {
    fontWeight: "bold",
    marginRight: 3,
  },
  drawerSection: {
    marginTop: 15,
  },
  bottomDrawerSection: {
    marginBottom: 15,
    borderTopColor: "#f4f4f4",
    borderTopWidth: 1,
  },
  preference: {
    flexDirection: "row",
    justifyContent: "space-between",
    paddingVertical: 12,
    paddingHorizontal: 16,
  },
});
