import React from "react";
import { createDrawerNavigator } from "@react-navigation/drawer";
import { DrawerContent } from "./DrawerContent";
import HomeTab from "./HomeTab";
import { About, Account, Product, Profile, Details } from "../Screens";

const Drawer = createDrawerNavigator();
const HomeStacks = ({ navigation }) => {
  return (
    <Drawer.Navigator
      drawerType="slide"
      drawerContent={(props) => <DrawerContent {...props} />}
    >
      <Drawer.Screen name="HomeDrawer" component={HomeTab} />
      <Drawer.Screen name="Profile" component={Profile} />
      <Drawer.Screen name="Account" component={Account} />
      <Drawer.Screen name="Product" component={Product} />
      <Drawer.Screen name="About" component={About} />
      <Drawer.Screen name="Details" component={Details} />
    </Drawer.Navigator>
  );
};
export default HomeStacks;
