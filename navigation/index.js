import React from "react";
import {
  NavigationContainer,
  DarkTheme as NavigationDarkTheme,
  DefaultTheme as NavigationDefaultTheme,
} from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import Splash from "../Screens/SplashScreen";
import IntroSlider from "../Screens/IntroSliderScreen";
import HomeStacks from "./HomeStacks";
import { AuthContext } from "../components/context";
import {
  SignIn,
  SignUp,
  Terms,
  EditProfile,
  Forgot,
  FAQ,
  Contact,
  Verify,
} from "../Screens";
import {
  Provider as PaperProvider,
  DarkTheme as PaperDarkTheme,
  DefaultTheme as PaperDefaultTheme,
} from "react-native-paper";

const MainAppStack = createStackNavigator();

const MainApp = () => {
  const [isDarkTheme, setIsDarkTheme] = React.useState(false);

  const CustomDefaultTheme = {
    ...NavigationDefaultTheme,
    ...PaperDefaultTheme,
    colors: {
      ...NavigationDefaultTheme.colors,
      ...PaperDefaultTheme.colors,
    },
  };

  const CustomDarkTheme = {
    ...NavigationDarkTheme,
    ...PaperDarkTheme,
    colors: {
      ...NavigationDarkTheme.colors,
      ...PaperDarkTheme.colors,
    },
  };

  const theme = isDarkTheme ? CustomDarkTheme : CustomDefaultTheme;

  const authContext = React.useMemo(
    () => ({
      toggleTheme: () => {
        setIsDarkTheme((isDarkTheme) => !isDarkTheme);
      },
    }),
    []
  );

  return (
    <PaperProvider theme={theme}>
      <AuthContext.Provider value={authContext}>
        <NavigationContainer theme={theme}>
          <MainAppStack.Navigator
            screenOptions={{ header: () => false }}
            initialRouteName={Splash}
          >
            <MainAppStack.Screen name="Splash" component={Splash} />
            <MainAppStack.Screen name="IntroSlider" component={IntroSlider} />
            <MainAppStack.Screen name="HomeStack" component={HomeStacks} />
            <MainAppStack.Screen name="SignIn" component={SignIn} />
            <MainAppStack.Screen name="SignUp" component={SignUp} />
            <MainAppStack.Screen name="Terms" component={Terms} />
            <MainAppStack.Screen name="Forgot" component={Forgot} />
            <MainAppStack.Screen name="Verify" component={Verify} />
            <MainAppStack.Screen name="EditProfile" component={EditProfile} />
            <MainAppStack.Screen name="FAQ" component={FAQ} />
            <MainAppStack.Screen name="Contact" component={Contact} />
          </MainAppStack.Navigator>
        </NavigationContainer>
      </AuthContext.Provider>
    </PaperProvider>
  );
};

export default MainApp;
