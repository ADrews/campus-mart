import { StyleSheet } from "react-native";

const DefaultStyles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#07485B",
  },
  header: {
    flex: 1,
    justifyContent: "flex-end",
    paddingHorizontal: 20,
    paddingBottom: 50,
  },
  title: {
    fontWeight: "bold",
    fontSize: 24,
    color: "white",
  },
  footer: {
    flex: 3,
    backgroundColor: "white",
    borderTopRightRadius: 30,
    borderTopLeftRadius: 30,
    paddingHorizontal: 30,
    paddingVertical: 50,
  },
  Submit: {
    backgroundColor: "white",
    height: 45,
    shadowColor: "black",
    borderRadius: 10,
    alignItems: "center",
    justifyContent: "center",
    elevation: 5,
  },
  Forgot: {
    marginTop: 20,
    marginBottom: 20,
    flexDirection: "row",
    justifyContent: "center",
  },
  SubmitTxt: {
    fontSize: 22,
    color: "#07485B",
    fontWeight: "bold",
  },
  Termstxt: {
    fontSize: 14,
    color: "black",
    marginBottom: 25,
  },
  TermsTitle: {
    fontSize: 18,
    textDecorationLine: "underline",
    marginBottom: 10,
  },
  preference: {
    flexDirection: "row",
    margin:20,
    alignItems: "center",
  },
});

export default DefaultStyles;
