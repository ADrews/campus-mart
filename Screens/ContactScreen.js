import React, { Component } from "react";
import { Text, View, ScrollView, Image } from "react-native";
import BackHeader from "../components/BackHeader";
import { Icons } from "../constants/Image";
import DefaultStyle from "../constants/Styles/DefaultStyles";
import { Card } from "native-base";
import { Avatar } from "react-native-paper";
import { Contact } from "../constants/Image";

const { TermsTitle } = DefaultStyle;

const StyledInfo = ({ name, number, email, icon }) => {
  return (
    <Card style={{ flexDirection: "row" }}>
      <Avatar.Image source={icon} size={70} />
      <View>
        <Text style={{ marginLeft: 10 }}>{name}</Text>
        <Text style={{ marginLeft: 10 }}>{number}</Text>
        <Text style={{ marginLeft: 10 }}>{email}</Text>
      </View>
    </Card>
  );
};

const Styledheader = ({ title }) => {
  return (
    <View style={{ alignItems: "center" }}>
      <Text style={TermsTitle}>{title}</Text>
    </View>
  );
};

const ContactScreen = () => {
  return (
    <View style={{ backgroundColor: "white" }}>
      <View style={{ alignItems: "center" }}>
        <Text style={{ fontSize: 20, marginTop: 20, color: "black" }}>
          Contact
        </Text>
      </View>
      <ScrollView
        style={{
          margin: 20,
          backgroundColor: "#FFF",
          borderRadius: 20,
          marginBottom: 100,
        }}
      >
        <Text style={{ alignItems: "center" }}>
          Below are the Brains behind KiwiMart
        </Text>
        <Styledheader title="FrontEnd" />
        <StyledInfo
          icon={Contact.Felix}
          name="FELIX OPPONG KWADWO"
          number="0240286066"
          email="oppongfelix880@gmail.com"
        />
        <StyledInfo
          icon={Contact.Andy}
          name="ASAMOAH ANDREWS"
          number="0557462789"
          email="kasamoah715@gmail.com"
        />
        <Styledheader title="BackEnd" />
        <StyledInfo
          icon={Contact.Confi}
          name="Antwi Confidence"
          number="0543524033"
          email="antwiconfidence@gmail.com"
        />
        <StyledInfo
          icon={Contact.Akandi}
          name="AKANDI MICHAEL"
          number="0246782984"
          email="akandimichael@yahoo.com"
        />
        <StyledInfo
          icon={Contact.Bash}
          name="ABDUL BASHIRU"
          number="0540633840"
          email="abdulbashiru.ba25@gmail.com"
        />
        <Styledheader title="Documentation" />
        <StyledInfo
          icon={Contact.Cedric}
          name="CEDRIC OBLITEY COMMEY"
          number="0572316846"
          email="ccedriccommey@gmail.com"
        />
        <StyledInfo
          icon={Contact.Afreh}
          name="AFREH CHRISTIAN BOATENG"
          number="0550678330"
          email="afrehchristian@gmail.com"
        />
        <View style={{ flexDirection: "row", justifyContent: "center" }}>
          <Image
            source={Icons.twitter}
            width={20}
            height={20}
            style={{ marginRight: 10 }}
          />
          <Image
            source={Icons.facebook}
            width={20}
            height={20}
            style={{ marginRight: 10 }}
          />
          <Image source={Icons.whatsApp} width={10} height={10} />
        </View>
      </ScrollView>
    </View>
  );
};
export default ContactScreen;
