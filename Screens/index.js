import Home from "./HomeScreen.js";
import About from "./AboutScreen";
import Account from "./AccountScreen";
import Product from "./ProductScreen";
import SignIn from "./SignInScreen";
import SignUp from "./SignUpScreen";
import Splash from "./SplashScreen";
import Terms from "./Terms&Conditions";
import Forgot from "./ForgotScreen";
import Settings from "./SettingsScreen";
import Profile from "./ProfileScreen";
import Categories from "./CategoryScreen";
import EditProfile from "./EditProfile";
import Contact from "./ContactScreen";
import FAQ from "./FAQScreen";
import Intro from "./IntroSliderScreen";
import Details from "./Detail";
import Verify from "./PasswordVer";

export {
  Splash,
  Home,
  About,
  Settings,
  EditProfile,
  Account,
  Categories,
  Profile,
  Product,
  SignIn,
  SignUp,
  Terms,
  Forgot,
  FAQ,
  Details,
  Intro,
  Verify,
  Contact,
};
