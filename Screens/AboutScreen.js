import React from "react";
import { Text, View, TouchableOpacity, ScrollView } from "react-native";
import BackHeader from "../components/BackHeader";
import DefaultStyle from "../constants/Styles/DefaultStyles";
const { container, TermsTitle, Termstxt } = DefaultStyle;
import { useTheme } from "react-native-paper";

const TextInputStyle = ({ Label, CStyle }) => {
  return (
    <View>
      <Text style={[TermsTitle, { color: CStyle }]}>{Label}</Text>
    </View>
  );
};

const StyledContent = ({ Label, CStyle }) => {
  return (
    <View>
      <Text style={[Termstxt, { color: CStyle }]}>{Label}</Text>
    </View>
  );
};

const AboutScreen = ({ navigation }) => {
  const { colors } = useTheme();
  state = {
    showTerms: true,
  };
  return (
    <View style={container}>
      <View style={{ marginLeft: 10 }}>
        <TouchableOpacity onPress={() => navigation.goBack()}>
          <BackHeader />
        </TouchableOpacity>
      </View>
      <View style={{ alignItems: "center", justifyContent: "center" }}>
        <Text style={{ fontSize: 20, marginTop: 30, color: "white" }}>
          About
        </Text>
        <View
          style={[
            {
              margin: 10,
              backgroundColor: "#FFF",
              borderRadius: 20,
              marginBottom: 180,
            },
            { backgroundColor: colors.background },
          ]}
        >
          <ScrollView style={{ margin: 10 }}>
            <StyledContent
              Label={
                "KiwiMart is basically an apllication for buying and selling in a convenient and effortless manner."
              }
              CStyle={colors.text}
            />
            <StyledContent
              Label={"The software is currently in Beta version"}
              CStyle={colors.text}
            />
            <StyledContent
              Label={
                "The app is developed by Kiwi Kodex,third year enginnering group of KNUST."
              }
              CStyle={colors.text}
            />
            <StyledContent
              Label={
                "The software is currently being worked upon and was launched in 2020."
              }
              CStyle={colors.text}
            />
          </ScrollView>
        </View>
      </View>
    </View>
  );
};
export default AboutScreen;
