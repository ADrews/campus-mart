import React, { useEffect, useState, Component } from "react";
import {
  TextInput,
  ActivityIndicator,
  Text,
  View,
  ImageBackground,
  StyleSheet,
  TouchableOpacity,
  ScrollView,
  AsyncStorage,
} from "react-native";
import { IMAGE } from "../constants/Image";
import { Feather } from "@expo/vector-icons";
import { Formik } from "formik";
import * as yup from "yup";
import Constants from "expo-constants";
import * as ImagePicker from "expo-image-picker";
import * as Permissions from "expo-permissions";

const StyledInput = ({ label, formikProps, formikKey, ...rest }) => {
  const inputStyles = {
    borderBottomWidth: StyleSheet.hairlineWidth,
    alignItems: "flex-start",
    height: 20,
    fontSize: 16,
    borderBottomColor: "blue",
    marginBottom: 10,
  };
  if (formikProps.touched[formikKey] && formikProps.errors[formikKey]) {
    inputStyles.borderBottomColor = "red";
  }
  return (
    <View>
      <Text style={styles.Title}>{label}</Text>
      <View style={inputStyles}>
        <TextInput
          style={{ fontSize: 16, flex: 1 }}
          onChangeText={formikProps.handleChange(formikKey)}
          onBlur={formikProps.handleBlur(formikKey)}
          {...rest}
        />
      </View>
      <Text style={{ color: "red" }}>
        {formikProps.touched[formikKey] && formikProps.errors[formikKey]}
      </Text>
    </View>
  );
};

const phoneRegExp = /^(\+?\d{0,4})?\s?-?\s?(\(?\d{3}\)?)\s?-?\s?(\(?\d{3}\)?)\s?-?\s?(\(?\d{4}\)?)?$/;
const validationSchema = yup.object().shape({
  email: yup.string().label("Email").email("Invalid Email").required(),
  Username: yup.string().label("Username").required().min(2, "Too Short!"),
  University: yup.string().label("University").required(),
  Contact: yup
    .string()
    .required()
    .matches(phoneRegExp, "Phone number is invalid")
    .min(10, "Number not complete")
    .max(13),
  Location: yup.string().label("Location").required(),
});

//

//

export default class EditProfileScreen extends Component {
  render() {
    const [email, setEmail] = useState("");
    const [mobileNumber, setMobile] = useState("");

    const [address_1, setAddr1] = useState("");
    const [university, setUniversity] = useState("");
    const [userName, setUserName] = useState("");

    const [userID, setUserID] = useState("");

    useEffect(() => {
      async function fetchToken() {
        const token = await AsyncStorage.getItem("token");

        fetch("http://127.0.0.1:3000/profile", {
          headers: new Headers({
            Authorization: "Bearer " + token,
          }),
        })
          .then((res) => res.json())
          .then((data) => {
            console.log(data);
            setEmail(data.email);
            setUserName(data.userName);
            setMobile(data.mobileNumber);
            setAddr1(data.address);
            setUniversity(data.university);
            setUserID(data.userId);
          });
      }

      fetchToken();
    }, []);

    return (
      <Formik
        initialValues={{
          Username: "",
          University: "",
          email: "",
          Contact: "",
          Location: "",
        }}
        onSubmit={(values, actions) => {
          alert(JSON.stringify(values));
          setTimeout(async () => {
            actions.setSubmitting(false);

            /*
    
              fetch("http://192.168.42.114:3000/edit", {
          method:"PUT",
          headers: {
            'Content-Type': 'application/json'
          },
          body:JSON.stringify({
             
              email:email,
              userName:userName,
              userID:userID
              
    
          })
    
        })
          .then(res=>res.json())
          .then(async (data)=>{
            console.log(data);
            try {
              await AsyncStorage.setItem('token', data.token);
              console.log("Edited");
              props.navigation.replace("home");
            } catch (e) {
              // saving error
              console.log("error editing profile:", e);
            }    
          })
          */
          }, 1000);
        }}
        validationSchema={validationSchema}
      >
        {(formikProps) => (
          <View style={styles.container}>
            <View style={styles.header}>
              <ImageBackground source={IMAGE.shoe} style={styles.image}>
                <View style={{ alignItems: "flex-end", marginRight: 20 }}>
                  <TouchableOpacity style={styles.icon}>
                    <Feather name="camera" color="white" size={25} />
                  </TouchableOpacity>
                </View>
              </ImageBackground>
            </View>
            <View style={styles.footer}>
              <ScrollView>
                <StyledInput
                  label="Username"
                  formikProps={formikProps}
                  initialValues={userName}
                  value={userName}
                  formikKey="Username"
                />
                <StyledInput
                  label="Email"
                  formikProps={formikProps}
                  initialValues={email}
                  value={email}
                  formikKey="email"
                  placeholder="kiwiKodex@gmail.com"
                />
                <StyledInput
                  label="Contact"
                  formikProps={formikProps}
                  formikKey="Contact"
                  initialValues={mobileNumber}
                  value={mobileNumber}
                  placeholder="contact"
                  keyboardType="numeric"
                />
                <StyledInput
                  label="University"
                  formikProps={formikProps}
                  formikKey="University"
                  initialValues={university}
                  value={university}
                />
                <StyledInput
                  label="Address"
                  formikProps={formikProps}
                  formikKey="Location"
                  initialValues={address_1}
                  value={address_1}
                  placeholder="Kotei"
                />
              </ScrollView>
              {formikProps.isSubmitting ? (
                <ActivityIndicator />
              ) : (
                <View style={{ alignItems: "center" }}>
                  <TouchableOpacity
                    style={styles.Submit}
                    onPress={formikProps.handleSubmit}
                  >
                    <Text style={styles.Title}>Save</Text>
                  </TouchableOpacity>
                </View>
              )}
            </View>
          </View>
        )}
      </Formik>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#07485B",
  },
  header: {
    flex: 1,
    backgroundColor: "white",
    justifyContent: "center",
  },
  Title: {
    fontSize: 18,
    fontWeight: "bold",
    color: "black",
    marginBottom: 3,
  },
  footer: {
    flex: 2,
    backgroundColor: "white",
    justifyContent: "center",
    paddingHorizontal: 30,
    paddingVertical: 50,
  },
  Submit: {
    backgroundColor: "white",
    height: 45,
    width: 70,
    shadowColor: "black",
    borderRadius: 10,
    alignItems: "center",
    justifyContent: "center",
    elevation: 5,
  },
  image: {
    flex: 1,
    resizeMode: "contain",
    justifyContent: "center",
  },
  icon: {
    marginTop: 100,
    backgroundColor: "#07485B",
    borderRadius: 50,
    height: 35,
    width: 35,
    alignItems: "center",
    justifyContent: "center",
  },
  text: {
    fontSize: 16,
    color: "grey",
    marginBottom: 10,
  },
});
