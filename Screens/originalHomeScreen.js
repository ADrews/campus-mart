import React from "react";
import Searchbar from "../components/Searchbar";
import { StyleSheet } from "react-native";
import { Container, Content, Card, View, Text } from "native-base";
import ContentSwiper from "../components/ContentSwiper";
import ItemsCard from "../components/ItemsCard";
import PomadeCard from "../components/PomadeCard";
import LaptopsCard from "../components/LaptopsCard";
import { LaptopsData, PomadeData } from "../components/Data";

const HomeScreen = () => {
  return (
    <Container>
      <Searchbar />
      <Content style={{ marginTop: 70, backgroundColor: "#d5d5d5" }}> 
        <ContentSwiper />
        <Card>
          <Text style={styles.Title}>Latest Deals</Text>
          <ItemsCard />
        </Card>
        <Card>
          <Text style={styles.Title}>Pomade</Text>
          <PomadeCard data={PomadeData} />
        </Card>
        <Card>
          <Text style={styles.Title}>Laptops</Text>
          <LaptopsCard data={LaptopsData} />
        </Card>
      </Content>
    </Container>
  );
};
export default HomeScreen;

const styles = StyleSheet.create({
  Title: { fontWeight: "bold", margin: 10 },
});
