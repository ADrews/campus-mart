import React, { Component } from "react";
import {
  View,
  ImageBackground,
  StyleSheet,
  TouchableOpacity,
  Text,
  Dimensions,
  AsyncStorage
} from "react-native";
import { IMAGE } from "../constants/Image";
import { Feather } from "@expo/vector-icons";
import { Avatar, TextInput } from "react-native-paper";

import * as ImagePicker from "expo-image-picker";

const { height, width } = Dimensions.get("screen");

export default class EditProfileScreen extends Component {
  constructor(props) {
    super(props);

    this.state = {
      img: null,
      imDisplay: null,
      visible: false,
      email: this.props.route.params.email,
      mobileNumber: this.props.route.params.mobileNumber,
      address_1: this.props.route.params.address_1,
      profileImage:this.props.route.params.profileImage
      

    };
  }
  componentDidMount() {
    //Here is the Trick
    const { navigation } = this.props;
    //Adding an event listner om focus
    //So whenever the screen will have focus it will set the state to zero
    this._unsubscribe = navigation.addListener("focus", () => {
      // do something

      this.setState({
        img: null,
        imDisplay: null,
        visible: false,
        email: this.props.route.params.email,
        mobileNumber: this.props.route.params.mobileNumber,
        address_1: this.props.route.params.address_1,
        profileImage:this.props.route.params.profileImage
      });
    });
  }

  componentWillUnmount() {
    // Remove the event listener before removing the screen from the stack

    this._unsubscribe();
  }

 async editProfile(){
  const userId=  await AsyncStorage.getItem('userId');

    if(this.state.img == null){

      fetch("http://192.168.42.114:3000/edit", {
            method:"PUT",
            headers: {
              'Content-Type': 'application/json'
            },
            body:JSON.stringify({
               
                email:this.state.email,
              
                mobileNumber:this.state.mobileNumber,
                address_1:this.state.address_1,
                userID:userId
            })
    
          })
            .then(res=>res.json())
            .then(async (data)=>{
              console.log(data);
              try {
                await AsyncStorage.setItem('token', data.token);
                alert("profile Updated");
            this.props.navigation.navigate("Home");
              } catch (e) {
                // saving error
                console.log("error editing profile:", e);
              }    
            })

    }


    else{

      let apiUrl = "http://192.168.42.114:3000/editProfile";

      let uri = this.state.img.uri;
  
      let uriParts = uri.split(".");
      let fileType = uriParts[uriParts.length - 1];
  
      let formData = new FormData();
      formData.append("profileImage", {
        uri,
        name: `profileImage.${fileType}`,
        type: `image/${fileType}`,
      });
  
      formData.append("email", this.state.email);
      formData.append("mobileNumber", this.state.mobileNumber);
      formData.append("address_1", this.state.address_1);
      formData.append("userID", userId);
      let options = {
        method: "POST",
        body: formData,
        headers: {
          Accept: "application/json",
          "Content-Type": "multipart/form-data",
        },
      };
  
      fetch(apiUrl, options)
        .then((res) => res.json())
        .then(async (data) => {
          console.log(data);
          try {
            alert("profile Updated");
            this.props.navigation.navigate("Home");
          } catch (e) {
            // saving error
            alert(e);
          }
        });

    }

    
  }

  showModal = () => {
    this.setState({ visible: true });
  };

  handleCancel = (e) => {
    console.log(e);
    this.setState({ visible: false });
  };
  _pickImage = async () => {
    try {
      let result = await ImagePicker.launchImageLibraryAsync({
        mediaTypes: ImagePicker.MediaTypeOptions.Images,
        allowsEditing: true,
        aspect: [4, 3],
        quality: 1,
      });
      if (!result.cancelled) {
        this.setState({ image: result.uri });
        this.setState({ imDisplay: result });
        this.setState({ img: result });
      }

      //console.log(this.state.img);
    } catch (E) {
      console.log(E);
    }
  };
  render() {
    let { image } = this.state;
    return (
      <View style={styles.container}>
        <View style={styles.header}>
          <ImageBackground style={{ flex: 1 }} source={IMAGE.background}>
            <View style={styles.containerImg}>
              <View style={styles.imageContainer}>
                {image && <Avatar.Image source={{ uri: image }} size={100} />}
              </View>
            </View>
            <View style={{ alignItems: "flex-end", marginRight: 10 }}>
              <TouchableOpacity style={styles.icon} onPress={this._pickImage}>
                <Feather name="camera" color="white" size={25} />
              </TouchableOpacity>
            </View>
          </ImageBackground>
        </View>
        <View style={styles.footer}>
          <TextInput 
          label="Email" 
          value={this.state.email}
          onChangeText={(text)=>{this.setState({email:text})}}
          
          
          style={{ backgroundColor: "white" }} />
          <TextInput 
          label="Contact" 
          value={this.state.mobileNumber}
          onChangeText={(text)=>{this.setState({mobileNumber:text})}}
          style={{ backgroundColor: "white" }} />
          <TextInput 
          label="Location" 
          value={this.state.address_1}
          onChangeText={(text)=>{this.setState({address_1:text})}}
          style={{ backgroundColor: "white" }} />
          
          <TouchableOpacity onPress={()=>this.editProfile(this.state)} style={styles.Submit}>
            <Text>Save</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#07485B",
  },
  header: {
    flex: 1,
    backgroundColor: "white",
    justifyContent: "center",
  },
  Title: {
    fontSize: 18,
    fontWeight: "bold",
    color: "black",
    marginBottom: 3,
  },
  footer: {
    flex: 2,
    backgroundColor: "white",
    justifyContent: "center",
    paddingHorizontal: 20,
  },
  Submit: {
    backgroundColor: "white",
    height: 45,
    width: 70,
    shadowColor: "black",
    marginTop: 20,
    marginLeft: 50,
    borderRadius: 10,
    alignItems: "center",
    justifyContent: "center",
    elevation: 5,
  },
  image: {
    flex: 1,
    resizeMode: "contain",
    justifyContent: "center",
  },
  icon: {
    backgroundColor: "#07485B",
    borderRadius: 50,
    height: 35,
    width: 35,
    alignItems: "center",
    justifyContent: "center",
  },
  imageContainer: {
    borderWidth: 1,
    borderColor: "#FFF",
    width: "40%",
    height: "80%",
    borderRadius: 100,
  },
  text: {
    fontSize: 16,
    color: "grey",
    marginBottom: 10,
  },
  containerImg: {
    marginTop: 10,
    alignItems: "center",
  },
});
