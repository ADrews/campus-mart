import React from "react";
import {
  Text,
  View,
  StyleSheet,
  TouchableOpacity,
  Dimensions,
  ActivityIndicator,
  ScrollView,
  AsyncStorage
} from "react-native";
import { Formik } from "formik";
import * as yup from "yup";
import * as Animatable from "react-native-animatable";
import { TextInput } from "react-native-paper";
import { IMAGE } from "../constants/Image";

const StyledInput = ({ Label, formikProps, formikKey, ...rest }) => {
  const inputStyles = { borderColor: "red", marginBottom: 10 };
  if (formikProps.touched[formikKey] && formikProps.errors[formikKey]) {
    inputStyles.borderColor = "red";
  }
  return (
    <View>
      <TextInput
        style={styles.inputStyles}
        mode="outlined"
        label={Label}
        returnKeyType="next"
        onChangeText={formikProps.handleChange(formikKey)}
        onBlur={formikProps.handleBlur(formikKey)}
        {...rest}
      />
      <Text style={{ color: "red" }}>
        {formikProps.touched[formikKey] && formikProps.errors[formikKey]}
      </Text>
    </View>
  );
};

const validationSchema = yup.object().shape({
  code: yup.string().label("code").required("Enter Verification Code"),
  Password: yup
    .string()
    .label("Password")
    .required()
    .min(6, "Please enter a minimum of 6 characters"),
  VerifyPassword: yup
    .string()
    .label("VerifyPassword")
    .required()
    .min(6, "Password do not match"),
});

const PasswordVer = ({ navigation }) => {
  return (
    <Formik
      initialValues={{ code: "", Password: "", VerifyPassword: "" }}
      onSubmit={(values, actions) => {
      
        setTimeout(() => {
          actions.setSubmitting(false);
          if(values.Password != values.VerifyPassword){

            alert("Passwords do not match");
    
                }
    
              fetch("http://192.168.42.114:3000/reset", {
                method: "POST",
                headers: {
                  "Content-Type": "application/json",
                },
                body: JSON.stringify({
                  resetToken: values.code,
    
                  password: values.Password
                }),
              })
                .then((res) => res.json())
                .then(async (data) => {
                  console.log(data);
                  if (data.error) {
                    alert(data.error);
                  }
    
                  await AsyncStorage.setItem("token", data.token);
                  await AsyncStorage.setItem("userId", data.userID);
                  alert(data.msg);
                  navigation.navigate("SignIn");
                });

        }, 1000);
      }}
      validationSchema={validationSchema}
    >
      {(formikProps) => (
        <View style={styles.container}>
          <View style={styles.header}>
            <Animatable.Image
              animation="bounceIn"
              duration={2000}
              resizeMode="contain"
              style={styles.logo}
              source={IMAGE.Forgot}
            />
            <Text style={styles.title}>Forgot Password?</Text>
            <Text style={styles.txt}>
              Don't worry we just need your{"\n"}Verification code and new
              Password done!
            </Text>
          </View>
          <ScrollView style={styles.footer}>
            <Animatable.View animation="fadeInUpBig">
              <StyledInput
                Label="Code"
                formikProps={formikProps}
                formikKey="code"
              />
              <StyledInput
                Label="Password"
                formikProps={formikProps}
                formikKey="Password"
              />
              <StyledInput
                Label="Verify Password"
                formikProps={formikProps}
                formikKey="VerifyPassword"
              />

              {formikProps.isSubmitting ? (
                <ActivityIndicator />
              ) : (
                <TouchableOpacity style={styles.Submit}  onPress={formikProps.handleSubmit}>
                  <Text style={{ fontSize: 25, color: "#07485B" }}>
                    Reset Password
                  </Text>
                </TouchableOpacity>
              )}
            </Animatable.View>
          </ScrollView>
        </View>
      )}
    </Formik>
  );
};

const { height, width } = Dimensions.get("screen");
const logo_height = height * 0.28;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#07485B",
  },
  header: {
    flex: 2,
    justifyContent: "center",
    alignItems: "center",
  },
  title: {
    justifyContent: "flex-start",
    fontWeight: "bold",
    fontSize: 24,
    color: "white",
  },
  txt: {
    color: "red",
    textAlign: "center",
  },
  footer: {
    flex: 1,
    backgroundColor: "white",
    borderTopRightRadius: 30,
    borderTopLeftRadius: 30,
    paddingHorizontal: 30,
    paddingTop: 10,
    paddingBottom: 150,
    marginLeft: 5,
    marginRight: 5,
    elevation: 15,
  },
  Submit: {
    backgroundColor: "white",
    borderColor: "black",
    borderWidth: 0.5,
    height: 45,
    shadowColor: "black",
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 5,
    marginTop: 10,
    borderRadius: 5,
    marginBottom: 10,
    alignItems: "center",
    justifyContent: "center",
    elevation: 5,
  },
  Forgot: {
    flexDirection: "row",
    justifyContent: "center",
  },
  logo: {
    width: logo_height,
    height: logo_height,
  },
});
export default PasswordVer;
