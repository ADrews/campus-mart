import React,{Component} from "react";
import { StyleSheet, Text, View, TouchableOpacity, Image,Dimensions,AsyncStorage,Alert } from "react-native";
import { Avatar } from "react-native-paper";
import { IMAGE, Profile } from "../constants/Image";
import { MaterialIcons } from "@expo/vector-icons";

const { height, width } = Dimensions.get("screen");

export default class ProfileScreen extends Component {
                  constructor(props){

                      super(props);

                      this.state = {
                        fullName:"",
                        userName:"",
                        email:"",
                        mobileNumber:"",
                        university:"",
                        address_1:"",
                        profileImage:""
                      }
                  }

UNSAFE_componentWillMount() {
       this.fetchDetails();

       }



  async fetchDetails(){


    const token=  await AsyncStorage.getItem('token');
    
     
    fetch("http://192.168.42.114:3000/profile",{
      headers: new Headers({
        Authorization: "Bearer " + token
      })

      }).then(res=>res.json())
      .then(data=>{
        
        console.log(data);
        this.setState({
              fullName: data.fullName,
              userName:data.userName,
              email:data.email,
              mobileNumber:data.mobileNumber,
              university:data.university,
              address_1:data.address_1,
              profileImage:""
        });
        

      }
      
      )

  } 

  componentWillUnmount(){
    
  }

    render(){


      return (
        <View style={styles.container}>
          <View style={styles.header}>
            <View style={{ flexDirection: "row" }}>
              <Image source={Profile.styles2} style={{ width: 50, height: 50,margin:10 }} />
              <Image source={Profile.styles1} style={{ width: 40, height: 40,marginLeft:width-140 }} />
            </View>
            <View style={{ justifyContent: "center", alignItems: "center" }}>
              <Text style={{ fontSize: 20, color: "#000" }}>Profile Screen</Text>
              <Avatar.Image
                source={IMAGE.avatar}
                size={100}
                style={{ marginTop:10 }}
              />
            </View>
          </View>
          <View style={styles.footer}>
            <View style={{ marginLeft: 20 }}>
              <Text style={styles.Title}>Name</Text>
              <Text style={styles.text}>{this.state.fullName}</Text>
              <Text style={styles.Title}>Username</Text>
              <Text style={styles.text}>{this.state.userName}</Text>
              <Text style={styles.Title}>Email</Text>
              <Text style={styles.text}>{this.state.email}</Text>
              <Text style={styles.Title}>Contact</Text>
              <Text style={styles.text}>{this.state.mobileNumber}</Text>
              <Text style={styles.Title}>University</Text>
              <Text style={styles.text}>{this.state.university}</Text>
              <Text style={styles.Title}>Address</Text>
              <Text style={styles.text}>{this.state.address_1}</Text>
            </View>
            <View style={{ alignItems: "flex-start", marginRight: 20 }}>
              <View>
              <TouchableOpacity
                style={styles.Infoicon}
                onPress={() =>
                  this.props.navigation.navigate("EditProfile", {
              
              email:this.state.email,
              mobileNumber:this.state.mobileNumber,
              address_1:this.state.address_1,
              profileImage:this.state.profileImage
                  })
                }
              >
                <MaterialIcons name="edit" color="white" size={25} />
              </TouchableOpacity>
              </View>
            </View>
            <View style={{ alignItems: "flex-end", marginRight: 20 }}>
              <TouchableOpacity
                onPress={() =>
                  Alert.alert(
                    "Hey !",
                    "Are you sure you want to SignOut ?",
                    [
                      {
                        text: "Yes",
                        onPress: async () => {
                          try {
                            await AsyncStorage.removeItem("token");
                            await AsyncStorage.removeItem("userId");
                            this.props.navigation.replace("SignIn");
                          } catch (error) {
                            // Error retrieving data
                            console.log(error.message);
                          }
                        },
                      },
      
                      { text: "No", onPress: () => console.log("No Pressed") },
                    ],
                    { cancelable: false }
                  )
                }
              ><Text>Logout</Text>
              </TouchableOpacity>
              </View>
          </View>
        </View>
      );

    }
};



const styles = StyleSheet.create({
  container: {
    justifyContent: "center",
    alignContent: "center",
    flex: 1,
    backgroundColor: "#FFF",
  },
  header: {
    flex: 1,
    backgroundColor: "white",
    elevation: 15,
    borderBottomLeftRadius: 20,
    borderBottomRightRadius: 20,
    marginLeft: 10,
    marginRight: 10,
  },
  footer: {
    flex: 2,
    backgroundColor: "white",
    justifyContent: "center",
  },
  icon: {
    marginTop: 90,
    backgroundColor: "white",
    borderRadius: 50,
    height: 35,
    width: 35,
    alignItems: "center",
    justifyContent: "center",
  },
  Infoicon: {
    marginTop: 90,
    backgroundColor: "red",
    borderRadius: 50,
    height: 35,
    width: 35,
    alignItems: "center",
    justifyContent: "center",
    shadowColor: "black",
    elevation: 10,
  },
  Title: {
    fontSize: 20,
    fontWeight: "bold",
    color: "black",
  },
  text: {
    fontSize: 16,
    color: "grey",
    marginBottom: 10,
  },
});
