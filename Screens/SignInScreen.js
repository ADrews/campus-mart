import React, { useState } from "react";
import {
  TextInput,
  ActivityIndicator,
  Text,
  View,
  TouchableOpacity,
  AsyncStorage,
} from "react-native";
import { Formik } from "formik";
import * as yup from "yup";
import { Feather } from "@expo/vector-icons";
import DefaultStyle from "../constants/Styles/DefaultStyles";
import { useTheme } from "react-native-paper";

const {
  container,
  header,
  title,
  footer,
  Submit,
  Forgot,
  SubmitTxt,
} = DefaultStyle;

const StyledInput = ({
  label,
  formikProps,
  IconName,
  formikKey,
  Cstyle,
  CIcon,
  ...rest
}) => {
  const inputStyles = {
    borderWidth: 2,
    borderColor: "#17a3dd",
    borderRadius: 10,
    height: 50,
    alignItems: "center",
    flexDirection: "row",
  };
  if (formikProps.touched[formikKey] && formikProps.errors[formikKey]) {
    inputStyles.borderColor = "red";
  }
  return (
    <View>
      <Text style={[{ marginBottom: 3 }, { color: Cstyle }]}>{label}</Text>
      <View style={inputStyles}>
        <View style={{ marginRight: 10, marginLeft: 10 }}>
          <Feather name={IconName} color={CIcon} size={25} />
        </View>
        <TextInput
          style={{ fontSize: 18, flex: 1, paddingRight: 10 }}
          onChangeText={formikProps.handleChange(formikKey)}
          onBlur={formikProps.handleBlur(formikKey)}
          {...rest}
        />
      </View>
      <Text style={{ color: "red" }}>
        {formikProps.touched[formikKey] && formikProps.errors[formikKey]}
      </Text>
    </View>
  );
};

const validationSchema = yup.object().shape({
  email: yup.string().label("Email").email().required(),
  password: yup
    .string()
    .label("Password")
    .required()
    .min(6, "Please enter a minimum of 6 characters"),
});
//signup function

//end of signup function
const SignInScreen = ({ navigation }) => {
  const { colors } = useTheme();
  return (
    <Formik
      initialValues={{
        email: "",
        password: "",
      }}
      onSubmit={(values, actions) => {
        //alert(JSON.stringify(values));
        setTimeout(() => {
          actions.setSubmitting(false);
          //senCred(values);

          fetch("http://192.168.42.114:3000/signin", {
            method: "POST",
            headers: {
              "Content-Type": "application/json",
            },
            body: JSON.stringify({
              email: values.email,

              password: values.password,
            }),
          })
            .then((res) => res.json())
            .then(async (data) => {
              console.log(data);
              if (data.error) {
                alert(data.error);
              }

              await AsyncStorage.setItem("token", data.token);
              await AsyncStorage.setItem("userId", data.userID);
              alert("You are Logged in");
              navigation.navigate("Profile");
            });
        }, 1000);
      }}
      validationSchema={validationSchema}
    >
      {(formikProps) => (
        <View style={container}>
          <View style={header}>
            <Text style={title}>Welcome!</Text>
          </View>
          <View style={[footer, { backgroundColor: colors.background }]}>
            <StyledInput
              Cstyle={colors.text}
              CIcon={colors.text}
              label="Email"
              IconName="mail"
              formikProps={formikProps}
              formikKey="email"
              placeholder="kiwiKodex@gmail.com"
            />
            <StyledInput
              Cstyle={colors.text}
              CIcon={colors.text}
              label="Password"
              IconName="lock"
              formikProps={formikProps}
              formikKey="password"
              placeholder="password"
              secureTextEntry
            />
            <View style={Forgot}>
              <Text style={{ color: "grey" }}>Forgot your login details? </Text>
              <TouchableOpacity onPress={() => navigation.navigate("Forgot")}>
                <Text
                  style={[
                    {
                      color: "black",
                      fontWeight: "bold",
                    },
                    { color: colors.text },
                  ]}
                >
                  Get help signing in.
                </Text>
              </TouchableOpacity>
            </View>
            {formikProps.isSubmitting ? (
              <ActivityIndicator />
            ) : (
              <TouchableOpacity
                style={Submit}
                onPress={formikProps.handleSubmit}
              >
                <Text style={SubmitTxt}>Log in</Text>
              </TouchableOpacity>
            )}

            <View style={[Forgot, { paddingTop: 40 }]}>
              <Text style={{ color: "grey" }}>Don't have an account? </Text>
              <TouchableOpacity onPress={() => navigation.navigate("SignUp")}>
                <Text
                  style={[
                    { color: "black", fontWeight: "bold" },
                    { color: colors.text },
                  ]}
                >
                  Sign Up
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      )}
    </Formik>
  );
};
export default SignInScreen;
