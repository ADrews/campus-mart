import React from "react";
import { Text, View, TouchableOpacity, ScrollView } from "react-native";
import DefaultStyle from "../constants/Styles/DefaultStyles";
import BackHeader from "../components/BackHeader";
const { container, TermsTitle, Termstxt } = DefaultStyle;

const FAQScreen = ({navigation}) => {
  return (
    <View style={container}>
      <View style={{ marginLeft: 10 }}>
        <TouchableOpacity onPress={() => navigation.goBack()}>
          <BackHeader />
        </TouchableOpacity>
      </View>
      <View style={{ alignItems: "center", justifyContent: "center" }}>
        <Text style={TermsTitle}>Frequently Asked Questions</Text>
        <View
          style={{
            margin: 20,
            backgroundColor: "#FFF",
            borderRadius: 20,
            marginBottom: 150,
          }}
        >
          <ScrollView style={{ margin: 10 }}>
            <Text style={Termstxt}>Q1. Where can i get the application</Text>
            <Text style={Termstxt}>
              At the moment ,the app is now on playstore.
            </Text>
            <Text style={Termstxt}>Q2. Does the app requires payments</Text>
            <Text style={Termstxt}>
              The app itself doesnot require payment but payments are made to
              the product owners.
            </Text>
          </ScrollView>
        </View>
      </View>
    </View>
  );
};
export default FAQScreen;
